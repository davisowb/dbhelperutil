
# Variables for setup (these must be string only!)
__module_name__ = u'dbhelperutil'
__description__ = u'A collection of DB Helper utilities.'

__version__ = u'1.1.3'

__author__ = u'Oli Davis'
__authorshort__ = u'OWBD'
__authoremail__ = u'oli.davis@me.com'

__license__ = u'MIT'

__githost__ = u'bitbucket.org'
__gituser__ = u'davisowb'
__gitrepo__ = u'dbhelperutil.git'


# Additional variables
__copyright__ = u'Copyright (C) 2016 {author}'.format(author=__author__)

__url__ = u'https://{host}/{user}/{repo}'.format(host=__githost__,
                                                 user=__gituser__,
                                                 repo=__gitrepo__)
__downloadurl__ = u'{url}/get/{version}.tar'.format(url=__url__,
                                                    version=__version__)
